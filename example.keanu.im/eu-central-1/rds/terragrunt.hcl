# Include all settings from the root terragrunt.hcl file
locals {
  name             = "rds"
  project          = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  rds_secrets_path = "${local.project.locals.data_dir}/matrix-rds.sops.yml"
  rds              = yamldecode(sops_decrypt_file(local.rds_secrets_path))
  namespace        = local.project.locals.namespace
  environment      = local.project.locals.environment
  pg_name          = replace("${local.namespace}${local.environment}${local.name}", "/[^a-zA-Z0-9]/", "")
  aws_profile      = get_env("AWS_PROFILE")
}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//rds?ref=master"
}

dependencies {
  paths = ["../vpc", "../region-settings"]
}

dependency "vpc" {
  config_path = "../vpc"
}

dependency "region_settings" {
  config_path = "../region-settings"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name              = local.name
  instance_class    = local.rds.rds_instance_class
  allocated_storage = local.rds.rds_allocated_storage

  # network
  vpc_id         = dependency.vpc.outputs.vpc.vpc_id
  vpc_cidr_block = dependency.vpc.outputs.vpc.vpc_cidr_block
  subnet_ids     = dependency.vpc.outputs.rds_subnet_ids

  # creds
  admin_database_name     = local.pg_name
  admin_database_username = local.pg_name
  admin_database_password = local.rds.admin_password

  # provisioning the databases
  provisioner_subnet_id  = dependency.vpc.outputs.private_subnet_ids[0]
  session_manager_bucket = dependency.region_settings.outputs.ssm_playbook.ssm_logs_bucket
  playbooks_bucket       = dependency.region_settings.outputs.ssm_playbook.playbooks_bucket_id
  sops_rds_secrets_path  = local.rds_secrets_path
  databases              = local.rds.databases
  database_users         = local.rds.database_users
  aws_profile            = local.aws_profile
}
