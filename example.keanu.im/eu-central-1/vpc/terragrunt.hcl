# Include all settings from the root terragrunt.hcl file
locals {
  region = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  name   = "vpc"
}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//vpc?ref=master"
}


include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  az1        = local.region.locals.az1
  az2        = local.region.locals.az2
  cidr_block = local.region.locals.cidr_block
}
