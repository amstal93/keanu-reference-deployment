locals {
  aws_region = "eu-central-1"
  az1        = "eu-central-1a"
  az2        = "eu-central-1b"
  cidr_block = "10.55.0.0/16"
}
