# Include all settings from the root terragrunt.hcl file
locals {
}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//account-settings?ref=master"
}


include {
  path = find_in_parent_folders()
}

inputs = {
}
