# Include all settings from the root terragrunt.hcl file
locals {
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  region  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  name    = "internal-dns"
}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//route53-internal?ref=master"
}

dependency "vpc" {
  config_path = "../vpc"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  domain_name = "${local.project.locals.namespace}.${local.project.locals.environment}"
  vpcs = [
    {
      id     = dependency.vpc.outputs.vpc.vpc_id
      region = local.region.locals.aws_region
    }
  ]
}
