locals {
  data_dir       = "${get_parent_terragrunt_dir()}/data"
  project_vars   = yamldecode(file("${local.data_dir}/project.yml"))
  project        = local.project_vars.project
  aws_account_id = local.project_vars.aws_account_id
  account_name = local.project_vars.account_name
  aws_profile    = local.project_vars.aws_profile
  environment    = local.project_vars.environment
  is_prod_like   = local.project_vars.is_prod_like
  namespace      = local.project_vars.namespace
  tags = {
    "Project" = local.project
  }
}

